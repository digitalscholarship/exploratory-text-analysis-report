---
title: Visualize Topic Model 
output: 
  html_document:
    self_contained: false
    css: ./include/lda.css
    includes:
      in_header: "header.html"
---

<div id="lda">

<script>
new LDAvis('#lda', './include/ldavis.json')
</script>

</div>

