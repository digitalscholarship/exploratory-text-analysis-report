---
title: Sentiment Analysis
output: html_document
---

```{r include=FALSE, initializing}
knitr::opts_chunk$set(echo = FALSE, message = FALSE, warning = FALSE)

library ("sentimentr")
library ("dplyr")
library ("plotly")
library ("kableExtra")

source("../src/sentiment.R")

# load corpus
# note doesn't read in lemma corpus even if available
obj_corpus = readRDS(file="../results_data/rds/cleanCorpus.Rds")


pprint = function(table){
  # Print a kable centered with column width shrunk to fit
  knitr::kable(table, format = 'html', align = 'l') %>%
  kable_styling(bootstrap_options = 'striped', full_width = F)
}


```


Sentiment analysis is the systematic identification of affective states and subjective information. The output of running sentiment analysis over a document is a numerical score. A negative score indicates negative sentiment, a score of 0 means absolute neutrality, and a positive score reflects positive sentiment. The further away from 0 a score is, the more polarized the sentiment. So for example both a strongly negative text and a mildly negative text will receive a sentiment score of less than zero, the strongly negative one will score further away from zero (in this case, lower) than the mildly negative one. For details on how the sentiment score was computed, [see here](https://github.com/trinker/sentimentr).

This section contains the following:

- **Summary statistics about sentiment distribution in the corpus.** These provide information about the minimum and maximum sentiment scores in the corpus, along with values for median, mean, and quartile intervals.
- **Sentiment score frequency histogram.** This graph describes the counts of documents in the corpus that received a given sentiment scores. For example the mode of the sentiment distribution will have the highest bar in the graph.
- **Word frequencies by sentiment.** These word frequency lists report the most frequent content words in 2 sub-corpora: the **Positive** subcorpus contains all documents from the corpus which had a positive sentiment score, while the **Negative** sub-corpus contains all documents which had a negative sentiment score.

<hr>

<h4 style="text-align:center">Numeric Summary</h4>
```{r sentiment summary}

sentiments = get_sentiments(obj_corpus)

df = get_summary(sentiments)

pprint(df)
```

<h4 style="text-align:center">Boxplot (Outliers Eliminated)</h4>
```{r sentiment boxplot, fig.width = 10}

plotly::plot_ly(x = sentiments, type = "box", name = "Sent.")
```

<h4 style="text-align:center">Histogram</h4>
```{r sentiment histogram, fig.width = 10}

plot_ly(x = sentiments,
        type = "histogram") %>%
  layout(xaxis = list(title = "Sentiment Score"),
         yaxis = list(title = "Frequency"))
```

