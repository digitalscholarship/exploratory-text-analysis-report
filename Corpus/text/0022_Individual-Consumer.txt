The definition for the term "Natural" food should include the following:

First, it should mean that genetic engineering was limited to selective breeding. No genetic splicing or other modification outside of traditional, typical, naturally-occurring plant or animal reproductive processes (as appropriate to whether the food-stock is plant or animal based).

Next, it should mean that any additives or chemicals used are naturally-occurring minerals (such as salt) that can be purchased by household cooks without any need for a special license, and/or are easily extracted from edible plants or animals by household cooks via common household cooking methods.

Natural should also mean that flavors and colors used in food products come from sources limited by the two previous restrictions.
