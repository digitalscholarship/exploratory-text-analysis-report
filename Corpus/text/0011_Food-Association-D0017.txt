To quote Robert Lustig from The New York Times:

"Perhaps the best way to define 'natural' would be to define the amount of processing allowed 'post-harvesting.' For example, a food grown with pesticides could not be labeled 'organic' but it could be deemed 'natural.' Organic food processed with preservatives could not be labeled 'natural.'"
