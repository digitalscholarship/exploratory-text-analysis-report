Can there not be a multiple choice format or am I missing something?

This is an important issue and the questions are too multilayered and lengthy to address in a cut and paste manner. It seems that, much like the census surveys, we, the public, should be offered a more streamlined survey; further, one could argue that due to the nature of food's impact upon public health, this questionnaire should be mailed (or emailed) to the public. 
