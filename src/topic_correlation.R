library(NLP)
library(tm)
library(Hmisc)
library(corrplot)

setwd("~/programs/dsi/exploratory-text-analysis-report/src")
doc_topics = readRDS(file="../results_data/rds/topicDocs.Rds")

doc_topics_matrix = as.matrix(doc_topics)
doc_topics_correlation = as.matrix(Hmisc::rcorr(doc_topics_matrix))[[1]]

corrplot.mixed(doc_topics_correlation,
               lower = "circle",
               upper = "number",
               tl.col = "black",
               tl.cex = .6,
               number.cex = .6,
               mar = c(0,0,3,0),
               title = "Doc Topics Correlation")



