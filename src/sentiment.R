library(sentimentr)

#setwd("~/programs/dsi/exploratory-text-analysis-report/src")
#files = readRDS("../results_data/rds/cleanCorpus.Rds")

#plotly::plot_ly(x = sentiments.no.outliers, type = "box", name = "Sent.")
#plot_ly(x = sentiments,
#        type = "histogram") %>%
#  layout(xaxis = list(title = "Sentiment Score"),
#         yaxis = list(title = "Frequency"))

get_sentiments = function (files) {
  sentiments = sentimentr::sentiment_by(files$content, by = NULL)$ave_sentiment
  sentiments.no.outliers = remove_outliers(sentiments)
  sentiments.no.outliers = sentiments.no.outliers[!is.na(sentiments.no.outliers)]
  return (sentiments.no.outliers)
}
remove_outliers = function (x, na.rm=TRUE) {
  qnt = quantile(x, probs=c(.25, .75), na.rm=na.rm)
  H = 1.5 * IQR(x, na.rm=na.rm)
  y = x
  y[x<(qnt[1] - H)] = NA
  y[x>(qnt[2] + H)] = NA
  y
}

get_summary = function (sentiments) {
  # 5-number summary (min, 1st q, median, 3rd q, max)
  roundDigits = 4
  summary = round(unname(summary(sentiments))[-4], roundDigits)

  mean = round(mean(sentiments), roundDigits) #mean
  sd = round(sd(sentiments), roundDigits) #standard deviation
  skew = round(e1071::skewness(sentiments), roundDigits) #skewness

  # Bind into data frame
  values = c(summary, "-----", mean, sd, skew)
  names = c("Minimum", "1st Quartile", "Median", "3rd Quartile", "Maximum", "-----", "Mean", "Std. Deviation", "Skewness")
  df = data.frame(values)
  rownames(df) = names
  colnames(df) = NULL
  return (df)
}
